package Model;

import java.util.Comparator;

public class Person implements Measurable,Taxable,Comparable<Person>,Comparator<Person>{
	private String name;
	private double height;
	private double incomePerYear;
	private double tax;
	
	public Person(String na, double h){
		name = na;
		height = h;
	}
	
	public Person(double in){
		incomePerYear = in;
	}
	
	public double getIncomePerYear(){
		return incomePerYear;
	}
	
	public double getTax(){
		if (incomePerYear <= 300000){
			tax = incomePerYear * (0.05);
		}
		else{
			tax = (300000 * (0.05)) + ((incomePerYear-300000) * (0.1));
		}
		return tax;
	}
	
	@Override
	public double measure() {
		return height;
	}
	
	public int compareTo(Person other){
		if (incomePerYear < other.incomePerYear){return -1;}
		if (incomePerYear > other.incomePerYear){return 1;}
		return 0;
	}
	
	public int compare(Person o1, Person o2){		
		double p1 = o1.getTax();		
		double p2 = o2.getTax();
		
		if (p1 > p2) {return 1;}		
		if (p1 < p2) {return -1;}		
		return 0;		
	}
}
