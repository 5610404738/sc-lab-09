package Model;

import java.util.ArrayList;
import java.util.Collections;

public class Tester {

	public static void main(String[] args) {
		
		// --------- Person ----------
		
		ArrayList<Person> person = new ArrayList<Person>();
		Person person1 = new Person(100000);
		Person person2 = new Person(600000);
		Person person3 = new Person(500000);
		person.add(person1);
		person.add(person2);
		person.add(person3);
		
		System.out.println(" Before sort with Income per year : ");		
		for (Person pBefore : person) {			
		System.out.println(pBefore.getIncomePerYear() + " baht.");
		}
		
		Collections.sort(person);
		System.out.println("\n---- After sort with Income per year : ");
		for (Person pAfter : person) {
		System.out.println(pAfter.getIncomePerYear() + " baht.");		
		}
		
		// --------- Product ----------
		
		ArrayList<Product> product = new ArrayList<Product>();
		Product product1 = new Product("TV",5000);
		Product product2 = new Product("Radio",1000);
		Product product3 = new Product("Fan",3500);
		product.add(product1);
		product.add(product2);
		product.add(product3);
		
		System.out.println("\n Before sort with Price : ");		
		for (Product pBefore : product) {			
		System.out.println(pBefore.getPrice() + " baht.");
		}
	
		Collections.sort(product);
		System.out.println("\n---- After sort with Price : ");
		for (Product pAfter : product) {
		System.out.println(pAfter.getPrice() + " baht.");		
		}
		
		// --------- Company's Income ----------
		
		ArrayList<Company> income = new ArrayList<Company>();
		Company income1 = new Company("A", 100000, 50000);
		Company income2 = new Company("B", 10000, 5000);
		Company income3 = new Company("C", 200000, 100000);
		income.add(income1);
		income.add(income2);
		income.add(income3);
		
		System.out.println("\n Before sort with Income : ");		
		for (Company pBefore : income) {			
		System.out.println(pBefore.getIncome() + " baht.");
		}
		
		Collections.sort(income, new EarningComparator());
		System.out.println("\n---- After sort with Income : ");
		for (Company pAfter : income) {
		System.out.println(pAfter.getIncome() + " baht.");		
		}

		// --------- Company's Outcome ----------
		
		ArrayList<Company> outcome = new ArrayList<Company>();
		Company outcome1 = new Company("A", 100000, 50000);
		Company outcome2 = new Company("B", 10000, 5000);
		Company outcome3 = new Company("C", 200000, 100000);
		outcome.add(outcome1);
		outcome.add(outcome2);
		outcome.add(outcome3);
		
		System.out.println("\n Before sort with Outcome : ");		
		for (Company pBefore : outcome) {			
		System.out.println(pBefore.getOutcome() + " baht.");
		}
		
		Collections.sort(outcome, new ExpenseComparator());
		System.out.println("\n---- After sort with Outcome : ");
		for (Company pAfter : outcome) {
		System.out.println(pAfter.getOutcome() + " baht.");		
		}
		
		// --------- Company's Profit ----------
		
		ArrayList<Company> profit = new ArrayList<Company>();
		Company profit1 = new Company("A", 100000, 50000);
		Company profit2 = new Company("B", 10000, 5000);
		Company profit3 = new Company("C", 200000, 100000);
		
		profit.add(profit1);
		profit.add(profit2);
		profit.add(profit3);
		
		System.out.println("\n Before sort with Profit : ");		
		for (Company pBefore : profit) {			
		System.out.println(pBefore.getProfit() + " baht.");
		}
		
		Collections.sort(profit, new ProfitComparator());
		System.out.println("\n---- After sort with Profit : ");
		for (Company pAfter : profit) {
		System.out.println(pAfter.getProfit() + " baht.");		
		}
		
		// --------- All Sort ---------- 
		
		ArrayList<Taxable> all = new ArrayList<Taxable>();
		Company company = new Company("A", 100000, 50000);
		
		all.add(person1);
		all.add(product2);
		all.add(company);
		
		System.out.println("\n Before sort with All Tax : ");		
		for (Taxable pBefore : all) {			
		System.out.println(pBefore.getTax() + " baht.");
		}
		
		Collections.sort(all,new TaxAll());
		System.out.println("\n---- After sort with All Tax : ");
		for (Taxable pAfter : all) {
		System.out.println(pAfter.getTax() + " baht.");		
		}
		
	}

}
