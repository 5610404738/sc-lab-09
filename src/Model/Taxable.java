package Model;

public interface Taxable {
	double getTax();
}
