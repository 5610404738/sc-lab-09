package Model;

import java.util.Comparator;

public class TaxAll implements Comparator{
	public int compare(Object o1, Object o2){		
		double t1 = ((Taxable)o1).getTax();		
		double t2 = ((Taxable)o2).getTax();
		
		if (t1 > t2) {return 1;}		
		if (t1 < t2) {return -1;}		
		return 0;		
	}
	

}
